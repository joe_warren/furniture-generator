{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module Legs (
    Legs,
    legs, 
    benchLegs,
    tableLegs,
    symetricTableLegs
) where

import System.Random
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import qualified Csg

type Legs g = Double -> Double -> Rand g Csg.BspTree

legs :: RandomGen g => [Legs g]
legs = [boringLegs, midBarLegs, swivelChairLegsWithCastors, swivelChairLegsNoCastors, foldingLegs, archLegs]

benchLegs :: RandomGen g => [Legs g]
benchLegs =  [boringLegs, midBarLegs, foldingLegs]

tableLegs :: RandomGen g => [Legs g]
tableLegs =  [midBarLegs, foldingLegs, archLegs]

symetricTableLegs :: RandomGen g => [Legs g]
symetricTableLegs = [midBarLegs, swivelChairLegsWithCastors, swivelChairLegsNoCastors, foldingLegs, archLegs]

boringLegs :: RandomGen g => Legs g
boringLegs width depth = do
    height <- getRandomR (20.0, 40.0)
    legW <- getRandomR (0.5, 2.0)
    legD <- getRandomR (legW, 3.0)
    let leg = Csg.scale (legW, legD, height) Csg.unitCube 
    let positions = [(i*width, j*depth, 0.0) | i <- [-0.5, 0.5], j<- [-0.5, 0.5]]
    let legList = map (`Csg.translate` leg) positions
    let legs =  Csg.unionConcat legList
    beamHeight <- getRandomR(-height/4.0, height/4.0)
    let beam = Csg.translate (0.0,-depth/2.0, beamHeight) $ Csg.scale (width,legW,legD) Csg.unitCube

    return $ Csg.translate (0.0, 0.0, -height/2.0) $ legs `Csg.union` beam

midBarLegs :: RandomGen g => Legs g
midBarLegs width depth = do
    height <- getRandomR (20.0, 40.0)
    legW <- getRandomR (0.5, 2.0)
    legD <- getRandomR (legW, 3.0)
    let leg = Csg.scale (legD, legW, height) Csg.unitCube 
    let positions = [(i*width, j*depth, 0.0) | i <- [-0.5, 0.5], j<- [-0.5, 0.5]]
    let legs = map (`Csg.translate` leg) positions
    beamHeight <- getRandomR(-height/4.0, height/4.0)
    let beams = [Csg.translate (i*width,0, beamHeight) $ Csg.scale (legW,depth,legD) Csg.unitCube | i <-[-0.5, 0.5]]

    let crossBeam = Csg.translate (0,0, beamHeight) $ Csg.scale (width,legD,legW) Csg.unitCube 

    return $ Csg.translate (0.0, 0.0, -height/2.0) $ Csg.unionConcat (crossBeam : legs ++ beams)

swivelChairLegs :: RandomGen g => Bool -> Legs g
swivelChairLegs includeCastors width depth = do
    height <- getRandomR (25.0, 50.0)
    centerR1 <- getRandomR (1.5, 5.0)
    centerR2 <- getRandomR (1.2, centerR1)
    heightFeet <- getRandomR (1.0, 10.0)
    heightR1 <- getRandomR (5.0, (height -heightFeet)/2.0)
    let heightBeam = height - heightFeet
    let cylinder = Csg.unitCylinder 12
    let beamR2 = Csg.translate (0.0, 0.0, -heightBeam/2) $ Csg.scale(centerR2, centerR2, heightBeam) cylinder
    let beamR1 = Csg.translate (0.0, 0.0, heightR1/2-heightBeam) $ Csg.scale(centerR1, centerR1, heightR1) cylinder
    let beam = beamR1 `Csg.union` beamR2

    numFeet ::Int <- getRandomR(3, 8)
    let rotations = [(2.0 * pi * fromIntegral i)/fromIntegral numFeet | i <- [0..numFeet-1]]

    footPadding <- getRandomR (3.0, 6.0)
    let footLengthY = (max width depth) /2 + footPadding
    let footLength = sqrt ( footLengthY * footLengthY + heightFeet * heightFeet) 
    let footAngle = atan (heightFeet/footLengthY)
    footWidth <- getRandomR (1.5, centerR1)
    let footBeam = Csg.rotate (-1.0, 0.0, 0.0) footAngle $ Csg.translate (0.0, footLength/2.0, footWidth/2.0) $ Csg.scale (footWidth, footLength, footWidth) Csg.unitCube
    let footBeamList = map (\r -> Csg.rotate (0.0, 0.0, 1.0) r footBeam) rotations
    let foot = Csg.translate (0.0, 0.0, heightFeet - height) $ Csg.unionConcat footBeamList
    legOffsetRotation <- getRandomR (0, pi)
    includeBaseLegs <- case includeCastors of 
                    True -> return True
                    False -> getRandom
    baseLegHeight <- getRandomR (5, 10)
    let baseLegOffset = (0, (footLength*cos footAngle) + (footWidth*sin footAngle) - (footWidth/2), (-footLength*sin footAngle) + (footWidth*cos footAngle) - baseLegHeight/2 - height + heightFeet)
    baseLegs <- case includeBaseLegs of
                True -> do
                    let baseBeam = Csg.translate baseLegOffset $ Csg.scale (footWidth, footWidth, baseLegHeight) Csg.unitCube
                    let baseBeamList = map (\r -> Csg.rotate (0.0, 0.0, 1.0) r baseBeam) rotations
                    return $ Csg.unionConcat baseBeamList
                False -> return Csg.emptyBspTree
    castors <- case includeCastors of
                True -> do
                    castorRadius <- getRandomR (3, 6)
                    castorWidth <- getRandomR (footWidth * sqrt 2, footWidth*2.5)
                    let castor =Csg.translate (castorRadius - (footWidth/sqrt 2), 0, 0) $  Csg.rotate (1, 0, 0) (pi/2) $ Csg.scale (castorRadius, castorRadius, castorWidth) $ Csg.unitCylinder 10
                    let rotatedCastors = (\a -> Csg.rotate (0, 0, 1) a $ Csg.translate (0, 0, -baseLegHeight/2) $ Csg.translate baseLegOffset $ Csg.rotate (0, 0, -1) a castor) <$> rotations
                    return $ Csg.unionConcat rotatedCastors
                False -> return Csg.emptyBspTree
    let footPlusBase = foot `Csg.union` baseLegs `Csg.union` castors
    let rotatedFoot = Csg.rotate (0, 0, 1) legOffsetRotation footPlusBase 

    return $ beam `Csg.union` rotatedFoot

swivelChairLegsWithCastors :: RandomGen g => Legs g
swivelChairLegsWithCastors = swivelChairLegs True
swivelChairLegsNoCastors :: RandomGen g => Legs g
swivelChairLegsNoCastors = swivelChairLegs False
archLegs :: RandomGen g => Legs g
archLegs width depth = do
    heightPlus :: Double <- getRandomR (2.0, 8.0)
    let height :: Double = min 60 $ max width (depth/2) + heightPlus
    footSize <- getRandomR (2.0, 6.0)
    let widthR = (width/2.0) - footSize
    let depthR = (depth/2.0) - footSize
    let widthCyl = Csg.rotate (1, 0, 0) (pi/2) $ Csg.scale (widthR, widthR, depth+10.0) $ Csg.unitCylinder 10 
    let depthCyl = Csg.rotate (0, 1, 0) (pi/2) $ Csg.scale (depthR, depthR, width+10.0) $ Csg.unitCylinder 10
    let bothCyls = Csg.translate (0, 0, -height) (widthCyl `Csg.union` depthCyl)
    let cube = Csg.translate (0, 0, -height/2) $ Csg.scale (width, depth, height) Csg.unitCube
    return $ cube `Csg.subtract` bothCyls

foldingLegs :: RandomGen g => Legs g
foldingLegs width depth = do
    height <- getRandomR (20, 40)
    barWidth <- getRandomR (0.5, 2.0)
    let barLength = sqrt (depth * depth + height * height)
    let bar = Csg.scale (width, barWidth, barLength) Csg.unitCube `Csg.subtract` Csg.scale (width-2*barWidth, barWidth*2, barLength -2*barWidth) Csg.unitCube
    let angle = atan (depth/height)
    let part1 = Csg.translate (barWidth/2, 0,  -height/2) $ Csg.rotate (1,0,0) angle bar
    let part2 = Csg.translate (-barWidth/2, 0, -height/2) $ Csg.rotate (-1,0,0) angle bar
    return $ part1 `Csg.union` part2


