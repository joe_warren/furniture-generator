{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where
import qualified Data.Text.IO as T
import qualified Csg
import qualified Csg.STL
import Data.Serialize
import Data.Semigroup
import Options.Applicative as OA
import Data.Vec3 as V3
import System.Random
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import qualified Codec.Picture as P
import qualified Raytracer as RT
import RandomScene as RS
import qualified Objects

data Job = MeshJob | RaytraceJob {
    targetSize :: Int,
    samples :: Int, 
    parallelism :: Int
    }

data Opts = Opts
    { 
      generator :: !(IO StdGen),
      outFile :: String, 
      job :: Job
    }
optsParser :: ParserInfo Opts
optsParser =
    info
        (helper <*> programOptions)
        (fullDesc <> progDesc "Furniture Generator" <>
        header
            "Proc Gen Furniture: as either a raytraced render, or an STL mesh")
    where
        programOptions :: Parser Opts
        programOptions =
            Opts <$> (seedOptionDesc <|> ioOption) <*> outputOption <*> (meshOption <|> raytraceOption)

        seedOption :: Mod OptionFields String -> Parser (IO StdGen)
        seedOption = fmap (return . (fst.head.reads)) . strOption
        seedOptionDesc = seedOption (long "seed" <> metavar "VALUE"  <> help "Use a fixed seed")
        ioOption = flag' getStdGen ( long "random" <> help "Generate a random seed" )
        outputOption = strOption (long "output" <> metavar "VALUE"  <> help "File to write (prefix)" <> value "out")
        meshOption = flag' MeshJob ( long "mesh" <> help "Output an STL mesh" )
        targetSizeOption :: Parser Int
        targetSizeOption = OA.option auto ( long "targetsize"
           <> short 'x'
           <> metavar "N"
           <> help "Target size of the raytraced image"
           <> value 500  ) 
        samplesOption :: Parser Int
        samplesOption = OA.option auto ( long "samples"
           <> short 's'
           <> metavar "N"
           <> help "Number of samples per pixel to raytrace"
           <> value 500  ) 
        parallelismOption :: Parser Int
        parallelismOption = OA.option auto ( long "parallelism"
           <> short 'p'
           <> metavar "N"
           <> help "Number of sparks (threads) to render the image across"
           <> value 32 ) 
        raytraceFlag = flag' RaytraceJob ( long "raytrace" <> help "Output an image" ) 
        raytraceOption = raytraceFlag <*> targetSizeOption <*> samplesOption <*> parallelismOption

main :: IO ()
main = do
    opts <- execParser optsParser
    rng <- generator opts
    print $  next rng
    print "Generating objects"
    let (os, rng2) = runRand Objects.randomArrangement rng
    let o = Csg.fromTris $ concat $ Csg.toTris <$> os
    case job opts of
        MeshJob -> do 
            print "Writing mesh to file"
            let (o, _) = runRand Objects.randomItem rng
            T.writeFile ((++) (outFile opts) ".stl") $ Csg.STL.toSTL  o
        RaytraceJob tSize sampleCount parallelism -> do
            print "Raytracing"
            let (o, rng2) = runRand Objects.randomItems rng
            let ((w, h, scene), rng3) = runRand (RS.randomScene tSize sampleCount o) rng2
            let image = RT.generateImage parallelism w h scene rng3
            P.savePngImage ((++) (outFile opts) ".png") image 
