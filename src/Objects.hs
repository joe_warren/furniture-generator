{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Objects (
 chair, 
 bench, 
 table, 
 randomTable,
 randomItem,
 randomArrangement,
 randomItems
) where

import System.Random
import Data.Maybe
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import Choice
import qualified Csg
import qualified Arms
import qualified Legs
import qualified Seats
import qualified Backs

chair :: RandomGen g => Rand g Csg.BspTree
chair = abstractObject (getRandomR (20, 30)) (getRandomR (20, 30)) Legs.legs Seats.seats Backs.backs Arms.arms

bench :: RandomGen g => Rand g Csg.BspTree
bench = abstractObject (getRandomR (60, 90)) (getRandomR (20, 30)) Legs.benchLegs Seats.benchSeats Backs.backs Arms.arms

table :: RandomGen g => Int -> Int -> Rand g Csg.BspTree
table width depth = do
    unitSize <- getRandomR (45,55)
    let w = unitSize * fromIntegral width
    let d = unitSize * fromIntegral depth
    case width == depth of
      True -> abstractObject (return w) (return d) Legs.symetricTableLegs Seats.symetricTableTops Backs.noBacks Arms.noArms
      False -> abstractObject (return w) (return d) Legs.tableLegs Seats.tableTops Backs.noBacks Arms.noArms

randomTable :: RandomGen g => Rand g Csg.BspTree
randomTable = do
    w <- getRandomR (1, 3)
    h <- getRandomR (1, 3)
    table w h

randomItem :: RandomGen g => Rand g Csg.BspTree
randomItem = join $ choice [randomTable, chair, bench]

data SideOption = EmptySide | ChairSide | BenchSide 

sideOptionChoice :: RandomGen g => Int -> Rand g SideOption
sideOptionChoice 0 = return EmptySide
sideOptionChoice 1 = choice [EmptySide, ChairSide]
sideOptionChoice _ = choice [EmptySide, ChairSide, BenchSide]  

untupleMaybe :: (a, Maybe b) -> Maybe (a, b)
untupleMaybe (a, (Just b)) = Just (a, b)
untupleMaybe (a, Nothing) = Nothing

reifySide :: RandomGen g => SideOption -> Int -> Double -> Csg.BspTree -> Csg.BspTree -> Rand g [Csg.BspTree]
reifySide EmptySide _ _ _ _ = return []
reifySide BenchSide _ _ _ b = return [b]
reifySide ChairSide n len c _ = do
    let possiblyChair = do
                          b <- getRandom 
                          return $ case b of
                                    True -> Just c
                                    False -> Nothing
    unpositionedChairMaybes <- replicateM n possiblyChair
    let positions = [((0.5 + fromIntegral i)*len/(fromIntegral n)) - (len/2) | i <- [0..n-1]]

    let positionChairs = catMaybes $ untupleMaybe <$> zip positions unpositionedChairMaybes 
   
    return $ (\(p, c) -> Csg.translate (p, 0, 0) c) <$> positionChairs

level :: Csg.BspTree -> Csg.BspTree
level o = Csg.translate (0, 0, -h) o
  where 
    ((_,_,h), (_,_,_)) = Csg.aabb o

randomArrangement :: RandomGen g => Rand g [Csg.BspTree]
randomArrangement = do
    w <- getRandomR (1, 3)
    d <- getRandomR (1, 3)
    let lengths = [w, d, w, d]
    sides <- sequence $ sideOptionChoice <$> lengths
    theChair <- chair
    theBench <- bench
    table <- table w d
    let (wd, dd) = (\((x1, y1, z1), (x2, y2, z2)) -> (x1-x2, y1-y2)) $ Csg.aabb table
    let lenD  = [wd, dd, wd, dd]
    sides <- sequence $ (( uncurry $ uncurry reifySide) <$> (zip (zip sides lengths) lenD)) <*> [theChair] <*> [theBench]
    let tr = [(dd/2, 0), (wd/2, pi/2), (dd/2, pi), (wd/2, 3*pi/2)]
    let backOff = 0.5 * wd/fromIntegral w
    let positionSide = (\(s, (t, a)) ->((Csg.rotate (0,0,1) a).(Csg.translate (0, t+backOff, 0))) <$> s)
    let positionedSides = positionSide <$> (zip sides tr)
    return $ level <$> (table : (concat positionedSides))

randomItems :: RandomGen g => Rand g [Csg.BspTree]
randomItems = do
    b <- getRandom
    case b of
        True -> randomArrangement
        False -> (:[]) <$> chair

abstractObject :: RandomGen g => Rand g Double -> Rand g Double -> [Legs.Legs g] -> [Seats.Seat g] -> [Backs.Back g] -> [Arms.Arm g] -> Rand g Csg.BspTree
abstractObject widthR depthR legFns seatFns backFns armFns = do 
    width <- widthR
    depth <- depthR
    legsFn <- choice legFns 
    legs <- legsFn width depth
    seatFn <- choice seatFns
    seat <- seatFn width depth
    backFn <- choice backFns
    back <- backFn width
    armFn <- choice armFns
    arm <- armFn depth
    let bothArms = Arms.pluraliseArm width arm
    let backPositioned = Csg.translate (0.0, -depth/2, 0.0) back 
    return $ legs `Csg.union` seat `Csg.union` backPositioned `Csg.union` bothArms


