{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module RandomScene(
    randomScene 
) where

import System.Random
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import Choice
import qualified Data.Vec3 as V3
import qualified Colorschemes as CS
import qualified Raytracer as RT
import qualified Csg

type AABB = ((Double, Double, Double), (Double, Double, Double))

tuple3Min :: (Double, Double, Double) -> (Double, Double, Double) -> (Double, Double, Double)
tuple3Min (x1, y1, z1) (x2, y2, z2) = (min x1 x2, min y1 y2, min z1 z2)  


tuple3Max :: (Double, Double, Double) -> (Double, Double, Double) -> (Double, Double, Double)
tuple3Max (x1, y1, z1) (x2, y2, z2) = (max x1 x2, max y1 y2, max z1 z2)  

combineAABB :: AABB -> AABB -> AABB
combineAABB (min1, max1) (min2, max2) = (tuple3Min min1 min2, tuple3Max max1 max2)

groupAABB :: [Csg.BspTree] -> AABB
groupAABB os = foldl1 combineAABB $ Csg.aabb <$> os

centerSingleObject :: [Csg.BspTree] -> Csg.BspTree -> Csg.BspTree
centerSingleObject os = Csg.translate (-cx, -cy, -cz) 
  where
    ((x1, y1, z1), (x2, y2, z2)) = groupAABB os
    cx = (x1+x2)/2
    cy = (y1+y2)/2
    cz = (z1+z2)/2

center :: [Csg.BspTree] -> [Csg.BspTree]
center os = centerSingleObject os <$> os

cameraParams :: [Csg.BspTree] -> Int -> Int -> (Int, Int, Double)
cameraParams os maxD padding = (w+(2*padding), h+(2*padding), scale)
  where
    ((x1, y1, _), (x2, y2, _)) = groupAABB os
    dx = x2 - x1
    dy = y2 - y1
    m = max dx dy
    maxDunpad = maxD - (padding * 2)
    maxDf = fromIntegral $ maxDunpad
    scale = m/maxDf
    (w, h) = if (m == dx) then (maxDunpad, floor (maxDf*dy/dx)) else (floor (maxDf*dx/dy), maxDunpad)
 

rotateToIsoView :: Csg.BspTree -> Csg.BspTree
rotateToIsoView = tiltTowardsCamera . rotate45 . makeZup
  where
    tiltTowardsCamera = Csg.rotate (1, 0, 0) (pi/18) 
    rotate45 = Csg.rotate (0, -1, 0) (3*pi/4) 
    makeZup = Csg.rotate (-1, 0, 0) (pi/2)


randomScene :: RandomGen gen => Int -> Int -> [Csg.BspTree] -> Rand gen (Int, Int, RT.Scene)
randomScene tSize sampleCount objs = do
    let rotated = rotateToIsoView <$> objs
    let positioned = center rotated
    bg:itemColors <- CS.randomColors $ 1 + length objs
    useSpecularLight <- getRandom
    specularAmount <- getRandomR (0.3, 0.7)
    let specularLight = case useSpecularLight of
                          True -> [RT.Specular (specularAmount, specularAmount, specularAmount)]
                          False -> []
    let items = (\(r, c) -> RT.Item { 
        RT.itemObject = r, 
        RT.itemMaterials= [RT.Diffuse (CS.lighter 0.3 c)] ++ specularLight 
      }) <$> zip positioned itemColors
    lampX <- getRandomR (-100, 100)
    lampY <- getRandomR (-100, 100)
    lampZ <- getRandomR(100, 200)
    -- use a single unit cube to figure out where to put the lamp
    -- If it's stupid but it works, it isn't stupid
    -- :/
    let (_, (_, _, mz)) = groupAABB objs
    let lampGuideCube = Csg.translate (lampX, lampY,  mz + lampZ) Csg.unitCube
    let positionedGuideCube = centerSingleObject rotated $ rotateToIsoView lampGuideCube
    let (_, lampPos) = Csg.aabb positionedGuideCube

    let lamp = RT.SphereLight (20000.0, 20000.0, 20000.0) (V3.fromXYZ lampPos) 20 
    let (w, h, s) = cameraParams rotated tSize 20
    let camera = RT.isometricCamera w h 400 s
    return $ (w, h, RT.Scene { 
        RT.sceneItems = items,
        RT.sceneLights = [lamp],
        RT.sceneInfinity = (0.15, 0.15, 0.15),
        RT.sceneBackground = CS.darker 0.2 bg,
        RT.sceneAmbient = (0.05, 0.05, 0.05),
        RT.sceneCamera = camera,
        RT.recursionDepth = 2,
        RT.initialRecursionDepth = 2,
        RT.sampleCount = sampleCount })

