module Choice
    ( choice
    ) where

import System.Random
import Control.Monad
import Control.Monad.Random

-- I don't like using unsafe indexing here, but it works
-- And if we're being honnest, I had no choice
choice :: RandomGen gen => [a] -> Rand gen a 
choice xs = (xs !!) <$> getRandomR (0, length xs - 1)

