{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module Arms(
    Arm,
    arms,
    noArms,
    pluraliseArm
) where

import System.Random
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import qualified Csg

type Arm g = Double -> Rand g Csg.BspTree

arms :: RandomGen g => [Arm g]
arms = [noArm, boringArm, foldingArm, fancyArm]

noArms :: RandomGen g => [Arm g]
noArms = [noArm]

boringArm :: RandomGen g => Arm g
boringArm depth = do
    height <- getRandomR (8.0, 16.0)
    width <- getRandomR (3.0, 6.0)
    armDepth <- getRandomR (depth/2, depth)
    armrestHeight <- getRandomR (width/3, width)
    let armrest = Csg.translate (0, 0, height) $  Csg.scale (width, armDepth, armrestHeight) Csg.unitCube
    let armbeam = Csg.translate (0, 0, height/2.0) $ Csg.scale(width, width, height) Csg.unitCube
    return $ armrest `Csg.union` armbeam

fancyArm :: RandomGen g => Arm g
fancyArm depth = do
    height <- getRandomR (8.0, 16.0)
    width <- getRandomR (3.0, 6.0)
    radius <- getRandomR (width/2.0, width)
    beamWidth <- getRandomR (width/5.0, width/2.0)
    armDepth <- getRandomR (depth/2, depth)
    armrestHeight <- getRandomR (width/3, width)
    let armrest = Csg.translate (0, 0, height) $  Csg.scale (width, armDepth, armrestHeight) Csg.unitCube
    let armbeam = Csg.translate (0, 0, height/2.0) $ Csg.scale(beamWidth,beamWidth, height) Csg.unitCube
    numBeams :: Integer <- getRandomR (1, 4)
    let beamPositions = map (\i -> 0.8 * armDepth * ((fromIntegral i/fromIntegral numBeams) - 0.5)) [0..numBeams]
    let beams = map (\p -> Csg.translate (0.0, p, 0.0) armbeam) beamPositions
    let cylinder =  Csg.translate (radius - width/2.0, armDepth/2, height) $ Csg.scale (radius, radius, armrestHeight) $ Csg.unitCylinder 10
    return $ Csg.unionConcat $ armrest : cylinder : beams

foldingArm :: RandomGen g => Arm g
foldingArm depth = do
    height <- getRandomR (8.0, 16.0)
    width <- getRandomR (3.0, 6.0)
    armDepth <- getRandomR (depth/2, depth)
    armrestHeight <- getRandomR (width/3, width)
    beamWidth <- getRandomR (width/6, width/2)
    let beamHeight = sqrt $ height* height +  armDepth * armDepth
    let armrest = Csg.translate (0, 0, height) $  Csg.scale (width, armDepth, armrestHeight) Csg.unitCube
    let beamDepth = armDepth - beamWidth
    let beamHeight = sqrt $ beamDepth*beamDepth + height*height
    let armbeam = Csg.scale(beamWidth, beamWidth, beamHeight) Csg.unitCube
    let angle = atan $ beamDepth/height
    let beam1 = Csg.translate (beamWidth/2, 0, height/2) $ Csg.rotate (1, 0, 0) angle armbeam
    let beam2 = Csg.translate (-beamWidth/2, 0, height/2) $ Csg.rotate (-1, 0, 0) angle armbeam
    return $ armrest `Csg.union` beam1 `Csg.union` beam2

noArm :: RandomGen g => Arm g
noArm = const $ return Csg.emptyBspTree

pluraliseArm :: Double -> Csg.BspTree -> Csg.BspTree
pluraliseArm width arm = rightArm `Csg.union` leftArm
    where
        rightArm = Csg.translate (width/2, 0, 0) arm
        leftArm = Csg.scale (-1, 1, 1) rightArm


