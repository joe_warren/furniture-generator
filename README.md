# furniture-generator

![25 odd looking procedurally generated chairs](imgs/chairs.jpg)

Built using a CSG framework I'm working on, which is _much better_ than this project.

To run, clone this repository, and the Csg Framework, and the Raytracer into the same directory: 

- [The CSG framework is on BitBucket, here: https://bitbucket.org/joe_warren/csg-haskell](https://bitbucket.org/joe_warren/csg-haskell)
- [The Raytracer is also on BitBucket, here: https://bitbucket.org/joe_warren/csg-raytracer](https://bitbucket.org/joe_warren/csg-raytracer)

I'm sorry for not making this easier!

Then `cd` into this projects directory, and build and run using Haskell Stack:
```
   stack build
   stack exec furniture-generator-exe -- --random --mesh
```

Alternatively, the project also contains a raytracer, which can be used to generate rendered images of the furniture.
Fair warning, this is *really* slow; it turns out that a good raytracer is hard to write. 
```
   stack build
   stack exec furniture-generator-exe -- --random --raytrace
```
