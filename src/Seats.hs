{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module Seats (
    Seat,
    seats,
    benchSeats, 
    tableTops, 
    symetricTableTops
) where

import System.Random
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import qualified Csg

type Seat g =  Double -> Double -> Rand g Csg.BspTree

seats :: RandomGen g => [Seat g]
seats = [boringSeat, roundedSeat, circularSeat]

benchSeats :: RandomGen g => [Seat g]
benchSeats = [boringSeat, roundedSeat]

tableTops :: RandomGen g => [Seat g]
tableTops = [boringSeat, roundedSeat]

symetricTableTops :: RandomGen g => [Seat g]
symetricTableTops = [boringSeat, roundedSeat, circularSeat]

boringSeat :: RandomGen g => Seat g
boringSeat width depth = do 
    height <- getRandomR (0.5, 5.0)
    padding <- getRandomR (3.0, 6.0)
    return $ Csg.scale (width+padding, depth+padding, height) Csg.unitCube

roundedSeat :: RandomGen g => Seat g
roundedSeat width depth = do
    height <- getRandomR (0.5, 5.0)
    curvature <- getRandomR (3.0, 6.0)
    let boxA = Csg.scale (width, depth+curvature*2.0, height) Csg.unitCube
    let boxB = Csg.scale (width + curvature*2.0, depth, height) Csg.unitCube
    let cylinder = Csg.scale (curvature, curvature, height) $ Csg.unitCylinder 12
    let positions = [(i*width, j*depth, 0.0) | i <- [-0.5, 0.5], j<- [-0.5, 0.5]]
    let cylinderList = map (`Csg.translate` cylinder) positions
    let corners = Csg.unionConcat cylinderList
    return $ boxA `Csg.union` boxB  `Csg.union` corners

circularSeat :: RandomGen g => Seat g
circularSeat width depth = do
    height <- getRandomR (0.5, 5.0)
    padding <- getRandomR (3.0, 5.0)
    let r = padding + sqrt (width*width + depth*depth) /2.0
    return $ Csg.scale (r, r, height) $ Csg.unitCylinder 12


