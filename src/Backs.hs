{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module Backs (
    Back,
    backs, 
    noBacks
) where 

import System.Random
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import qualified Csg

type Back g = Double -> Rand g Csg.BspTree

backs :: RandomGen g => [Back g]
backs = [boringBack,roundedBack, horizontalBeamBack, verticalBeamBack]

noBacks :: RandomGen g => [Back g]
noBacks = [noBack]

noBack :: RandomGen g => Back g
noBack = const $ return Csg.emptyBspTree

reposeBack :: RandomGen g => Csg.BspTree -> Rand g Csg.BspTree
reposeBack back = do
    repose <- getRandomR (0, pi/8)
    return $ Csg.rotate (1.0, 0.0, 0.0) repose back

boringBack :: RandomGen g => Back g
boringBack width = do 
    height <- getRandomR (30, 40)
    depth <- getRandomR (0.5, 5.0)
    reposeBack $ Csg.translate (0.0, -depth/2, height/2) $ Csg.scale (width, depth, height) Csg.unitCube

roundedBack :: RandomGen g => Back g
roundedBack width = do 
    height <- getRandomR (30, 40)
    depth <- getRandomR (0.5, 5.0)
    curvature <- getRandomR (3.0, 6.0)
    let boxA = Csg.translate (0.0, -depth/2, height/2) $ Csg.scale (width-curvature*2, depth, height) Csg.unitCube
    let boxB = Csg.translate (0.0, -depth/2, (height-curvature)/2) $ Csg.scale (width, depth, height-curvature) Csg.unitCube

    let cylinder = Csg.rotate (1, 0, 0) (pi/2) $ Csg.scale (curvature, curvature, depth) $ Csg.unitCylinder 12
    let cylinders = Csg.unionConcat $ map (\i -> Csg.translate (i*(width/2-curvature), -depth/2, height-curvature) cylinder) [-1, 1]
    reposeBack $ Csg.unionConcat [boxA, boxB, cylinders] 

verticalBeamBack :: RandomGen g => Back g
verticalBeamBack width = do 
    height <- getRandomR (30, 40)
    depth <- getRandomR (0.5, 5.0)
    topHeight <- getRandomR (0.5, 10.0)
    let top = Csg.translate (0, 0, height -topHeight/2) $ Csg.scale (width, depth, topHeight) Csg.unitCube
    beamCount :: Integer <- getRandomR(3, 8)
    beamR <- getRandomR (0.5, min depth (width-depth)/fromIntegral beamCount)
    let singleBeam = Csg.scale (beamR, beamR, height) Csg.unitCube
    let beamPositionsOffset = map (\i -> (width-depth) * fromIntegral i/fromIntegral beamCount) [0..beamCount]
    let beamPositions = map (((width-depth)/2.0) -) beamPositionsOffset
    let beams = map (\p -> Csg.translate (p, 0, height/2.0) singleBeam ) beamPositions
    reposeBack $  Csg.unionConcat (top:beams) 

horizontalBeamBack :: RandomGen g => Back g
horizontalBeamBack width = do
    height <- getRandomR (30, 40)
    beamDepth <- getRandomR (3.0, 5.0)
    beamWidth <- getRandomR (beamDepth/4, beamDepth/2)
    let verticalBeam = Csg.scale (beamWidth, beamDepth, height) $ Csg.translate (0, 0, 0.5) Csg.unitCube
    let verticals = [Csg.translate (i*width/2, 0, 0) verticalBeam | i <- [-1, 1]]
    let horizontalBeam = Csg.scale (width, beamWidth, beamDepth) Csg.unitCube
    hBeamStart <- getRandomR (0, height/2)
    let hBeamRegion = height - hBeamStart
    hBeamCount :: Int <- getRandomR (2, floor $ hBeamRegion/beamDepth)
    let hBeams = [Csg.translate (0, 0, hBeamStart + (fromIntegral i-0.5)*hBeamRegion/fromIntegral hBeamCount) horizontalBeam | i <- [1..hBeamCount]]
    reposeBack $ Csg.unionConcat $ verticals ++ hBeams


